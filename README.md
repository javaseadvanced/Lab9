# Lab9
JavaBeans

#Содержание файла "MediaPriceChangeListener.java"

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class MediaPriceChangeListener implements PropertyChangeListener {

	public MediaPriceChangeListener() {
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

	}

}

#Содержание файла "MediaCategoryChangeListener.java"

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

public class MediaCategoryChangeListener implements VetoableChangeListener {

	public MediaCategoryChangeListener() {

	}

	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {

	}

}



#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "OnlineMedia build and run",
            "command": "javac -sourcepath /projects/Lab9/JavaBeans/src -d /projects/Lab9/JavaBeans/bin /projects/Lab9/JavaBeans/src/*.java && java -classpath /projects/Lab9/JavaBeans/bin OnlineMedia",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab9/JavaBeans/src",
                "component": "maven"
            }
        }
    ]
}
